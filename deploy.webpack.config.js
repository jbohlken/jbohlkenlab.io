module.exports = {
    mode: 'production',
    bail: true,
    target: 'web',
    entry: "./js/entry.js",
    output: {
        filename: '[name].bundle.js',
    },
    stats: {
        colors: true,
        reasons: true
    },
    module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
                loader: "babel-loader"
			}
		]
    },
    resolve: {
        extensions: ['.js', '.es6']
    },
    optimization: {
        minimize: true,
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        },
        runtimeChunk: {
            name: 'manifest'
        }
    }
};