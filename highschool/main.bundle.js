(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./js/createFullCalendar.js":
/*!**********************************!*\
  !*** ./js/createFullCalendar.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FullCalendar; });
/* harmony import */ var _fullcalendar_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @fullcalendar/core */ "./node_modules/@fullcalendar/core/main.esm.js");
/* harmony import */ var _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @fullcalendar/daygrid */ "./node_modules/@fullcalendar/daygrid/main.esm.js");
/* harmony import */ var _fullcalendar_list__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @fullcalendar/list */ "./node_modules/@fullcalendar/list/main.esm.js");
/* harmony import */ var _fullcalendar_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @fullcalendar/bootstrap */ "./node_modules/@fullcalendar/bootstrap/main.esm.js");
/* harmony import */ var _fullcalendar_google_calendar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @fullcalendar/google-calendar */ "./node_modules/@fullcalendar/google-calendar/main.esm.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }







var FullCalendar =
/*#__PURE__*/
function () {
  function FullCalendar() {
    _classCallCheck(this, FullCalendar);

    this.el = document.getElementById('full-calendar');

    if (this.el) {
      this.calendarIds = JSON.parse(this.el.dataset.calendars);

      if (this.calendarIds) {
        this.init();
        this.renderToolbar();
      }
    }
  }

  _createClass(FullCalendar, [{
    key: "init",
    value: function init() {
      var _this = this;

      var width = document.documentElement.clientWidth;
      this.calendar = new _fullcalendar_core__WEBPACK_IMPORTED_MODULE_0__["Calendar"](this.el, {
        plugins: [_fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_1__["default"], _fullcalendar_list__WEBPACK_IMPORTED_MODULE_2__["default"], _fullcalendar_bootstrap__WEBPACK_IMPORTED_MODULE_3__["default"], _fullcalendar_google_calendar__WEBPACK_IMPORTED_MODULE_4__["default"]],
        defaultView: width < 768 ? 'listYear' : 'dayGridMonth',
        themeSystem: 'bootstrap',
        header: {
          left: '',
          center: 'title',
          right: ''
        },
        displayEventTime: true,
        googleCalendarApiKey: 'AIzaSyBD04rpqHD1luVvxTIw5TdaEAn5AEszCUw',
        eventSources: this.calendarIds,
        viewSkeletonRender: function viewSkeletonRender(info) {
          console.log(info);
        },
        datesRender: function datesRender(info) {
          console.log(info);
        },
        windowResize: function windowResize(view) {
          if (document.documentElement.clientWidth < 768) {
            if (view.type !== 'listYear') {
              _this.calendar.changeView('listYear');
            }
          }
        },
        customButtons: {
          addToCalendar: {
            text: 'Subscribe to Calendar',
            click: function click() {
              alert('clicked the custom button!');
            }
          }
        }
      });
      this.calendar.render();
    }
  }, {
    key: "renderToolbar",
    value: function renderToolbar() {
      var _this2 = this;

      var toolbar = document.getElementById('full-calendar-toolbar');

      if (this.calendar && toolbar) {
        var prevEl = toolbar.querySelector('.fc-previous-button');
        var nextEl = toolbar.querySelector('.fc-next-button');
        var todayEl = toolbar.querySelector('.fc-today-button');
        var monthEl = toolbar.querySelector('.fc-dayGridMonth-button');
        var listEl = toolbar.querySelector('.fc-listYear-button');
        var subscribeEl = toolbar.querySelector('.fc-subscribe-to-calendar');

        if (prevEl) {
          prevEl.addEventListener('click', function (event) {
            event.preventDefault();

            _this2.calendar.prev();
          });
        }

        if (nextEl) {
          nextEl.addEventListener('click', function (event) {
            event.preventDefault();

            _this2.calendar.next();
          });
        }

        if (todayEl) {
          todayEl.addEventListener('click', function (event) {
            event.preventDefault();

            _this2.calendar.today();
          });
        }

        if (monthEl) {
          monthEl.addEventListener('click', function (event) {
            event.preventDefault();

            if (listEl) {
              listEl.classList.remove('active');
            }

            monthEl.classList.add('active');

            _this2.calendar.changeView('dayGridMonth');
          });
        }

        if (listEl) {
          listEl.addEventListener('click', function (event) {
            event.preventDefault();

            if (monthEl) {
              monthEl.classList.remove('active');
            }

            listEl.classList.add('active');

            _this2.calendar.changeView('listYear');
          });
        }

        if (subscribeEl) {
          subscribeEl.addEventListener('click', function (event) {
            event.preventDefault();
            var queryParams = new URLSearchParams();
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
              for (var _iterator = _this2.calendarIds[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var calendarId = _step.value;
                queryParams.append('cid', String(calendarId.googleCalendarId));
              }
            } catch (err) {
              _didIteratorError = true;
              _iteratorError = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator["return"] != null) {
                  _iterator["return"]();
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError;
                }
              }
            }

            window.open("https://calendar.google.com/calendar/r?".concat(queryParams.toString()));
          });
        }
      }
    }
  }]);

  return FullCalendar;
}();



/***/ }),

/***/ "./js/createowl.js":
/*!*************************!*\
  !*** ./js/createowl.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CreateOwl; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var CreateOwl =
/*#__PURE__*/
function () {
  function CreateOwl() {
    _classCallCheck(this, CreateOwl);

    this.$carousel = $('.owl-carousel');

    if (this.$carousel.length) {
      this.onLoad();
    }
  }

  _createClass(CreateOwl, [{
    key: "onLoad",
    value: function onLoad() {
      this.$carousel.owlCarousel({
        items: 1,
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        autoplay: true
      });
    }
  }]);

  return CreateOwl;
}();



/***/ }),

/***/ "./js/createslick.js":
/*!***************************!*\
  !*** ./js/createslick.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CreateSlick; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var CreateSlick =
/*#__PURE__*/
function () {
  function CreateSlick() {
    var _this = this;

    _classCallCheck(this, CreateSlick);

    this.$slider = $('.slider').not('.slick-initialized');

    if (this.$slider.length) {
      $(document).ready(function () {
        $(window).on('resize', _this.onLoad());
      });
      this.onLoad();
    }
  }

  _createClass(CreateSlick, [{
    key: "onLoad",
    value: function onLoad() {
      this.$slider.slick({
        vertical: true,
        autoplay: true,
        autoplaySpeed: 3000,
        arrows: false,
        pauseOnHover: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [{
          breakpoint: 576,
          settings: {
            vertical: false,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
      });
    }
  }]);

  return CreateSlick;
}();



/***/ }),

/***/ "./js/default.js":
/*!***********************!*\
  !*** ./js/default.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fetchCalendar__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fetchCalendar */ "./js/fetchCalendar.js");
/* harmony import */ var _fetchLandingSlideshow__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fetchLandingSlideshow */ "./js/fetchLandingSlideshow.js");
/* harmony import */ var _createslick__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./createslick */ "./js/createslick.js");
/* harmony import */ var _createowl__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./createowl */ "./js/createowl.js");
/* harmony import */ var _marchingBandVideo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./marchingBandVideo */ "./js/marchingBandVideo.js");
/* harmony import */ var _createFullCalendar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./createFullCalendar */ "./js/createFullCalendar.js");







(function () {
  $(document).ready(function () {
    var $window = $(window);
    var $header = $('#header');
    var pjaxContent = '#pjax-content';

    var updatePageNav = function updatePageNav() {
      var page = $('.primary-sections').data('page');
      $('li').removeClass('nav-item-active');
      $('#' + page + " li").addClass('nav-item-active');
    };

    var onReady = function onReady() {
      $header.addClass('loaded'); // Do the various page-specific things

      updatePageNav();
      new _createslick__WEBPACK_IMPORTED_MODULE_2__["default"]();
      new _createowl__WEBPACK_IMPORTED_MODULE_3__["default"]();
      new _fetchCalendar__WEBPACK_IMPORTED_MODULE_0__["default"]();
      new _fetchLandingSlideshow__WEBPACK_IMPORTED_MODULE_1__["default"]();
      new _createFullCalendar__WEBPACK_IMPORTED_MODULE_5__["default"]();
      var ticker = document.querySelector('.ticker');

      if (ticker) {
        var list = ticker.querySelector('.ticker-list');

        if (list) {
          var clone = list.cloneNode(true);
          ticker.append(clone);
        }
      }

      $('[data-toggle="tooltip"]').tooltip();
      var ytPromise = new Promise(function (onYouTubeIframeAPIReady) {
        var tag = document.createElement('script');
        var firstScriptTag = document.getElementsByTagName('script')[0];
        window.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady;
        tag.src = 'https://www.youtube.com/iframe_api';
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      });
      new _marchingBandVideo__WEBPACK_IMPORTED_MODULE_4__["default"](ytPromise);
    };

    $(document).pjax('a', pjaxContent, {
      fragment: pjaxContent
    }).on('pjax:end', function () {
      $window.triggerHandler('scroll');
      onReady();
    });
    setTimeout(onReady, 1);
  });
})();

/***/ }),

/***/ "./js/entry.js":
/*!*********************!*\
  !*** ./js/entry.js ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_polyfill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/polyfill */ "./node_modules/@babel/polyfill/lib/index.js");
/* harmony import */ var _babel_polyfill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_polyfill__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery_pjax__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery-pjax */ "./node_modules/jquery-pjax/jquery.pjax.js");
/* harmony import */ var jquery_pjax__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery_pjax__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! owl.carousel */ "./node_modules/owl.carousel/dist/owl.carousel.js");
/* harmony import */ var owl_carousel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(owl_carousel__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _default__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./default */ "./js/default.js");
/* harmony import */ var _createslick__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./createslick */ "./js/createslick.js");
/* harmony import */ var _createowl__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./createowl */ "./js/createowl.js");







/***/ }),

/***/ "./js/fetchCalendar.js":
/*!*****************************!*\
  !*** ./js/fetchCalendar.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FetchCalendar; });
/* harmony import */ var tiny_slider_src_tiny_slider__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tiny-slider/src/tiny-slider */ "./node_modules/tiny-slider/src/tiny-slider.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


var calendarIDs = {
  "events": "tttqah97hpn0rq07m1nf9po1to@group.calendar.google.com",
  "jazz1": "lqup8e65gb56ee9h3cj2bln71c@group.calendar.google.com",
  "jazz2": "n26pfql7419eji4hpq85f94uag@group.calendar.google.com",
  "mb": "kerh89jlutp69uq2mike7h5og8@group.calendar.google.com",
  "pep": "g91t5cecs0oiulpmt2lt73ssec@group.calendar.google.com",
  "perc": "rma153g34jd6ebl8o79kack9ng@group.calendar.google.com"
};
var apiKey = 'AIzaSyBD04rpqHD1luVvxTIw5TdaEAn5AEszCUw';

var FetchCalendar =
/*#__PURE__*/
function () {
  function FetchCalendar() {
    _classCallCheck(this, FetchCalendar);

    this.urls = [];
    this.slider = null;
    this.hasEvents = false;
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    this.today = today.toISOString();
    this.sidebar = document.getElementById('upcomingevents');

    if (this.sidebar) {
      var _this$sidebar$dataset, _this$sidebar$dataset2;

      this.eventsListUL = this.sidebar.querySelector('ul');
      this.calendars = JSON.parse((_this$sidebar$dataset = this.sidebar.dataset) === null || _this$sidebar$dataset === void 0 ? void 0 : _this$sidebar$dataset.calendars);
      var items = Number((_this$sidebar$dataset2 = this.sidebar.dataset) === null || _this$sidebar$dataset2 === void 0 ? void 0 : _this$sidebar$dataset2.slideItems);

      if (items) {
        this.items = items;
      } else {
        this.items = 5;
      }

      this.onLoad();
    }
  }

  _createClass(FetchCalendar, [{
    key: "onLoad",
    value: function onLoad() {
      var _this = this;

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = this.calendars[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var calendar = _step.value;

          if (calendarIDs[calendar]) {
            var url = "https://www.googleapis.com/calendar/v3/calendars/".concat(calendarIDs[calendar], "/events?key=").concat(apiKey, "&singleEvents=true&orderBy=startTime&timeMin=").concat(this.today);
            this.urls.push(url);
          } else {
            console.warn("[Attention] Calendar \"".concat(calendar, "\" is not in the list of available calendar IDs. Please double-check index.html for this page."));
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      var promises = this.urls.map(function (url) {
        return fetch(url).then(function (response) {
          return response.json();
        });
      });
      Promise.all(promises).then(function (results) {
        if (typeof results !== 'undefined') {
          var events = results.map(function (result) {
            return result.items;
          });
          var combinedEvents = [];
          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = events[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var event = _step2.value;

              for (var j = 0; j < event.length; j++) {
                combinedEvents.push(event[j]);
              }
            }
          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
                _iterator2["return"]();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }

          return combinedEvents;
        }
      }).then(function (events) {
        return events.sort(function (a, b) {
          var dateA = new Date(a.start.dateTime ? a.start.dateTime : a.start.date);
          var dateB = new Date(b.start.dateTime ? b.start.dateTime : b.start.date);
          return dateA - dateB;
        });
      }).then(function (events) {
        var values = {};
        return events.filter(function (event) {
          var time = new Date(event.start.dateTime ? event.start.dateTime : event.start.date);
          var value = event.summary + time;
          var exists = values[value];
          values[value] = true;
          return !exists;
        });
      }).then(function (events) {
        if (!events.length) {
          var li = document.createElement('li');
          li.textContent = 'No Upcoming Events';

          _this.eventsListUL.appendChild(li);
        } else {
          _this.hasEvents = true;
        }

        var _iteratorNormalCompletion3 = true;
        var _didIteratorError3 = false;
        var _iteratorError3 = undefined;

        try {
          for (var _iterator3 = events[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
            var event = _step3.value;
            // This is the loop that looks at the events. Build stuff here.
            var summary = event.summary;

            var startTime = _this.getDateString(event.start, {
              weekday: 'short',
              month: 'long',
              day: 'numeric',
              hour: 'numeric',
              minute: 'numeric'
            });

            var endTime = _this.getTimeString(event.end);

            var timeString = startTime;

            if (endTime !== '') {
              timeString = startTime + ' - ' + endTime;
            }

            var location = event.location;

            var _li = document.createElement('li');

            var summarySpan = document.createElement('span');
            summarySpan.className = 'news-item-summary';
            summarySpan.textContent = summary;

            _li.appendChild(summarySpan);

            _li.appendChild(document.createElement('br'));

            var time = document.createTextNode(timeString);

            _li.appendChild(time);

            if (location) {
              _li.appendChild(document.createElement('br'));

              var locationNode = document.createTextNode(location);

              _li.appendChild(locationNode);
            }

            _li.className = "news-item";

            _this.eventsListUL.appendChild(_li);
          }
        } catch (err) {
          _didIteratorError3 = true;
          _iteratorError3 = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
              _iterator3["return"]();
            }
          } finally {
            if (_didIteratorError3) {
              throw _iteratorError3;
            }
          }
        }

        return true;
      }).then(function () {
        // Instantiate the ticker here, after the list has been added and sorted
        if (_this.hasEvents) {
          _this.slider = Object(tiny_slider_src_tiny_slider__WEBPACK_IMPORTED_MODULE_0__["tns"])({
            container: '#events-list',
            axis: 'vertical',
            //items: 9,
            items: _this.items,
            //slideBy: 'page',
            controls: false,
            speed: 300,
            //default 300
            autoplay: true,
            autoplayHoverPause: true,
            autoplayTimeout: 5000,
            //default 5 sec
            autoplayButtonOutput: false,
            autoHeight: false,
            //default false (looks weird without)
            nav: false
          });
        }
      })["catch"](function (error) {
        throw new Error('Something went wrong while fetching calendars.', error.message);
      });
    }
  }, {
    key: "getDateString",
    value: function getDateString(key, options) {
      if (key.dateTime) {
        return new Date(key.dateTime).toLocaleDateString("en-US", options);
      } else {
        return new Date(key.date).toLocaleDateString("en-US", {
          weekday: 'short'
        });
      }
    }
  }, {
    key: "getTimeString",
    value: function getTimeString(key) {
      if (key.dateTime) {
        return new Date(key.dateTime).toLocaleTimeString("en-US", {
          hour: 'numeric',
          minute: 'numeric'
        });
      } else {
        return new Date(key.date).toLocaleTimeString("en-US", {
          hour: 'numeric',
          minute: 'numeric'
        });
      }
    }
  }]);

  return FetchCalendar;
}();



/***/ }),

/***/ "./js/fetchLandingSlideshow.js":
/*!*************************************!*\
  !*** ./js/fetchLandingSlideshow.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return FetchLandingSlideshow; });
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var apiKey = "6044fdf886586df6ecb52b9cf32b01e4";
var userId = "182343329@N06";
var baseUrl = "https://api.flickr.com/services/rest/?";

var FetchLandingSlideshow =
/*#__PURE__*/
function () {
  function FetchLandingSlideshow() {
    _classCallCheck(this, FetchLandingSlideshow);

    this.slideshowName = $('#landing-slideshow').data('slideshow');

    if (this.slideshowName) {
      this.onLoad();
    }
  }

  _createClass(FetchLandingSlideshow, [{
    key: "onLoad",
    value: function () {
      var _onLoad = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var _this = this;

        var photoSetUrlParams, response, data, set, photosUrlParams, photolist, photos, elems, baseSrc, suffix, i, farm, server, id, secret, title, src, _i, _Object$entries, _Object$entries$_i, index, el;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                photoSetUrlParams = new URLSearchParams({
                  method: "flickr.photosets.getList",
                  format: "json",
                  api_key: apiKey,
                  user_id: userId,
                  nojsoncallback: 1
                });
                _context.next = 3;
                return fetch(baseUrl + photoSetUrlParams.toString());

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.json();

              case 6:
                data = _context.sent;
                set = data.photosets.photoset.find(function (item) {
                  return item.title._content === _this.slideshowName;
                });
                photosUrlParams = new URLSearchParams({
                  method: "flickr.photosets.getPhotos",
                  format: "json",
                  api_key: apiKey,
                  user_id: userId,
                  photoset_id: set.id,
                  nojsoncallback: 1
                });
                _context.next = 11;
                return fetch(baseUrl + photosUrlParams.toString());

              case 11:
                photolist = _context.sent;
                _context.next = 14;
                return photolist.json();

              case 14:
                photos = _context.sent;
                elems = [];
                baseSrc = "https://c2.staticflickr.com";
                suffix = "_b.jpg";

                for (i = 0; i < photos.photoset.photo.length; i++) {
                  farm = String(photos.photoset.photo[i].farm);
                  server = String(photos.photoset.photo[i].server);
                  id = String(photos.photoset.photo[i].id);
                  secret = String(photos.photoset.photo[i].secret);
                  title = String(photos.photoset.photo[i].title);
                  src = "".concat(baseSrc, "/").concat(farm, "/").concat(server, "/").concat(id, "_").concat(secret).concat(suffix);
                  elems.push("<div class=\"item\" style=\"height: 300px; width: 100%; text-align: center;\"><img style=\"height: 100%; width: auto;\" src=\"".concat(src, "\" alt=\"").concat(title, "\"/></div>"));
                }

                for (_i = 0, _Object$entries = Object.entries(elems); _i < _Object$entries.length; _i++) {
                  _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2), index = _Object$entries$_i[0], el = _Object$entries$_i[1];
                  $("#landing-slideshow").append(el);
                }

                $("#landing-slideshow").addClass("owl-carousel").owlCarousel({
                  items: 1,
                  loop: true,
                  margin: 10,
                  nav: false,
                  dots: false,
                  autoplay: true
                });

              case 21:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function onLoad() {
        return _onLoad.apply(this, arguments);
      }

      return onLoad;
    }()
  }]);

  return FetchLandingSlideshow;
}();



/***/ }),

/***/ "./js/marchingBandVideo.js":
/*!*********************************!*\
  !*** ./js/marchingBandVideo.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return MarchingBandVideo; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var MarchingBandVideo =
/*#__PURE__*/
function () {
  function MarchingBandVideo(ytPromise) {
    _classCallCheck(this, MarchingBandVideo);

    this.tag = document.createElement('script');
    this.$firstScriptTag = $('script').get(0);
    this.$youtubeLightbox = $('#youtube-lightbox');
    this.$youtubeLightbox.on('click', $.proxy(this.hideLightbox, this));
    ytPromise.then(this.createLightbox());
  }

  _createClass(MarchingBandVideo, [{
    key: "onLoad",
    value: function onLoad() {
      this.tag.src = 'https://www.youtube.com/iframe_api';
      this.$firstScriptTag.parentNode.insertBefore(this.tag, this.$firstScriptTag);
    }
  }, {
    key: "createYoutubePlayer",
    value: function createYoutubePlayer(url) {
      this.player = new YT.Player('player-div', {
        videoId: url,
        playerVars: {
          autoplay: 1,
          modestbranding: 1,
          rel: 0,
          controls: 1
        },
        events: {
          onStateChange: $.proxy(this.onStateChange, this)
        }
      });
    }
  }, {
    key: "hideLightbox",
    value: function hideLightbox(event) {
      if (event.currentTarget !== $('.centered-child')) {
        var self = this;
        this.$youtubeLightbox.fadeOut(400, function () {
          self.$youtubeLightbox.find('iframe').remove();
          self.$youtubeLightbox.find('.video-wrapper').append('<div id="player-div"></div>');
        });
      }
    }
  }, {
    key: "onStateChange",
    value: function onStateChange(event) {
      if (event.data === 0) {
        this.$youtubeLightbox.fadeOut(400);
        this.$youtubeLightbox.find('iframe').remove();
        this.$youtubeLightbox.find('.video-wrapper').append('<div id="player-div"></div>');
      }
    }
  }, {
    key: "createLightbox",
    value: function createLightbox() {
      var self = this;
      $('body').on('click.YoutubeLightbox', 'a.lightbox', function (event) {
        event.preventDefault();
        var url = $(this).data('url');
        self.createYoutubePlayer(url);
        $('#youtube-lightbox').fadeIn(400);
      });
    }
  }]);

  return MarchingBandVideo;
}();



/***/ })

},[["./js/entry.js","manifest","vendors"]]]);
//# sourceMappingURL=main.bundle.js.map