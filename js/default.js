import FetchCalendar from './fetchCalendar';
import FetchLandingSlideshow from './fetchLandingSlideshow';
import CreateSlick from './createslick';
import CreateOwl from './createowl';
import MarchingBandVideo from './marchingBandVideo';

(function() {
    $(document).ready(function() {
        const $window = $(window);
        const $header = $('#header');
        const pjaxContent = '#pjax-content';
		
		const updatePageNav = () => {
			var page = $('.primary-sections').data('page');

			$('li').removeClass('nav-item-active');
			$('#' + page + " li").addClass('nav-item-active');
		};

        const onReady = () => {
            $header.addClass('loaded');

            // Do the various page-specific things
			updatePageNav();
			new CreateSlick();
			new CreateOwl();
			new FetchCalendar();
			new FetchLandingSlideshow();

			const ticker = document.querySelector('.ticker');

			if (ticker) {
			    const list = ticker.querySelector('.ticker-list');

			    if (list) {
			        const clone = list.cloneNode(true);
			        ticker.append(clone);
                }
            }

			$('[data-toggle="tooltip"]').tooltip();

			const ytPromise = new Promise(onYouTubeIframeAPIReady => {
				const tag = document.createElement('script');
				const firstScriptTag = document.getElementsByTagName('script')[0];

				window.onYouTubeIframeAPIReady = onYouTubeIframeAPIReady;

				tag.src = 'https://www.youtube.com/iframe_api';
				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			});

			new MarchingBandVideo(ytPromise);
        };

        $(document)
            .pjax('a', pjaxContent, { fragment: pjaxContent})
            .on('pjax:end', () => {
                $window.triggerHandler('scroll');
                onReady();
            });

        setTimeout(onReady, 1);
    })
})();