/* Jake's test */
/* 
const apiKey = "6044fdf886586df6ecb52b9cf32b01e4";
const userId = "182343329@N06";
*/

/* Band Flickr */
const apiKey = "6492b9f368c990fa8a8ac65ea7158eef;"
const userId = "183850002@N04";
const baseUrl = "https://api.flickr.com/services/rest/?";

export default class FetchLandingSlideshow {
    constructor() {
        this.slideshowName = $('#landing-slideshow').data('slideshow');

        if (this.slideshowName) {
            this.onLoad();
        }
    }

    async onLoad() {
        const photoSetUrlParams = new URLSearchParams({
            method: "flickr.photosets.getList",
            format: "json",
            api_key: apiKey,
            user_id: userId,
            nojsoncallback: 1
        });

        const response = await fetch(baseUrl + photoSetUrlParams.toString());

        const data = await response.json();
        const set = data.photosets.photoset.find(
            item => item.title._content === this.slideshowName
        );

        const photosUrlParams = new URLSearchParams({
            method: "flickr.photosets.getPhotos",
            format: "json",
            api_key: apiKey,
            user_id: userId,
            photoset_id: set.id,
            nojsoncallback: 1
        });

        const photolist = await fetch(baseUrl + photosUrlParams.toString());

        const photos = await photolist.json();

        const elems = [];

        const baseSrc = "https://c2.staticflickr.com";
        const suffix = "_b.jpg";

        for (let i = 0; i < photos.photoset.photo.length; i++) {
            const farm = String(photos.photoset.photo[i].farm);
            const server = String(photos.photoset.photo[i].server);
            const id = String(photos.photoset.photo[i].id);
            const secret = String(photos.photoset.photo[i].secret);
            const title = String(photos.photoset.photo[i].title);

            const src = `${baseSrc}/${farm}/${server}/${id}_${secret}${suffix}`;

            elems.push(
                `<div class="item" style="height: 300px; width: 100%; text-align: center;"><img style="height: 100%; width: auto;" src="${src}" alt="${title}"/></div>`
            );
        }

        for (const [index, el] of Object.entries(elems)) {
            $("#landing-slideshow").append(el);
        }

        $("#landing-slideshow")
            .addClass("owl-carousel")
            .owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
                nav: false,
                dots: false,
                autoplay: true
            });
    }
}
