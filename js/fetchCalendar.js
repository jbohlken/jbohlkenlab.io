import { tns } from 'tiny-slider/src/tiny-slider';

const calendarIDs = {
    "events": "tttqah97hpn0rq07m1nf9po1to@group.calendar.google.com",
    "jazz1": "lqup8e65gb56ee9h3cj2bln71c@group.calendar.google.com",
    "jazz2": "n26pfql7419eji4hpq85f94uag@group.calendar.google.com",
    "mb": "kerh89jlutp69uq2mike7h5og8@group.calendar.google.com",
    "pep": "g91t5cecs0oiulpmt2lt73ssec@group.calendar.google.com",
    "perc": "rma153g34jd6ebl8o79kack9ng@group.calendar.google.com"
};

const apiKey = 'AIzaSyBD04rpqHD1luVvxTIw5TdaEAn5AEszCUw';

export default class FetchCalendar {
	constructor() {
		this.urls = [];
		this.slider = null;
		this.hasEvents = false;

        const today = new Date();
        today.setHours(0,0,0,0);

        this.today = today.toISOString();

		this.sidebar = document.getElementById('upcomingevents');

		if (this.sidebar) {
			this.eventsListUL = this.sidebar.querySelector('ul');
			this.calendars = JSON.parse(this.sidebar.dataset?.calendars);

			const items = Number(this.sidebar.dataset?.slideItems);
			if (items) {
				this.items= items;
			} else {
				this.items = 5;						
			}									
									
			this.onLoad();
		}
	}

	onLoad() {
        for (const calendar of this.calendars) {
            if (calendarIDs[calendar]) {
                const url = `https://www.googleapis.com/calendar/v3/calendars/${calendarIDs[calendar]}/events?key=${apiKey}&singleEvents=true&orderBy=startTime&timeMin=${this.today}`;
                this.urls.push(url);
            } else {
                console.warn(`[Attention] Calendar "${calendar}" is not in the list of available calendar IDs. Please double-check index.html for this page.`);
            }
        }
        const promises = this.urls.map(url => fetch(url).then(response => response.json()));

        Promise.all(promises).then(results => {
            if (typeof results !== 'undefined') {
                const events = results.map(result => result.items);
                const combinedEvents = [];

                for (const event of events) {
                    for (let j = 0; j < event.length; j++) {
                        combinedEvents.push(event[j]);
                    }
                }
                return combinedEvents;
            }
        })
        .then(events => {
            return events.sort((a, b) => {
                let dateA = new Date(a.start.dateTime ? a.start.dateTime : a.start.date);
                let dateB = new Date(b.start.dateTime ? b.start.dateTime : b.start.date);
                return dateA - dateB;
            });
        }).then(events => {
            let values = {};
            return events.filter(event => {
                let time = new Date(event.start.dateTime ? event.start.dateTime : event.start.date);
                let value = event.summary + time;
                let exists = values[value];
                values[value] = true;
                return !exists;
            });
        }).then(events => {
            if (!events.length) {
                const li = document.createElement('li');
				li.textContent = 'No Upcoming Events';
                this.eventsListUL.appendChild(li);
            } else {
				this.hasEvents = true;
			}

            for (const event of events) {
                // This is the loop that looks at the events. Build stuff here.
                const summary = event.summary;
                const startTime = this.getDateString(event.start, {
                    weekday: 'short',
                    month: 'long',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric'
                });
                const endTime = this.getTimeString(event.end);

                let timeString = startTime;

                if (endTime !== '') {
                	timeString = startTime + ' - ' + endTime;
				}

                const location = event.location;

                const li = document.createElement('li');

                const summarySpan = document.createElement('span');
                summarySpan.className = 'news-item-summary';
                summarySpan.textContent = summary;

                li.appendChild(summarySpan);
                li.appendChild(document.createElement('br'));

                const time = document.createTextNode(timeString);
                li.appendChild(time);

                if (location) {
                	li.appendChild(document.createElement('br'));

                	const locationNode = document.createTextNode(location);
                	li.appendChild(locationNode);
				}

                li.className = "news-item";
                this.eventsListUL.appendChild(li);
            }

            return true;
        }).then(() => {
            // Instantiate the ticker here, after the list has been added and sorted
			if (this.hasEvents) {
				this.slider = tns({
                container: '#events-list',
                axis: 'vertical',
                //items: 9,
				items: this.items,
                //slideBy: 'page',
                controls: false,
                speed: 300, //default 300
                autoplay: true,
                autoplayHoverPause: true,
                autoplayTimeout: 5000, //default 5 sec
                autoplayButtonOutput: false,
                autoHeight: false, //default false (looks weird without)
                nav: false
            });
			}
		})
        .catch(error => {
            throw new Error('Something went wrong while fetching calendars.', error.message);
        })
	}

	getDateString(key, options) {
        if (key.dateTime) {
            return new Date(key.dateTime).toLocaleDateString("en-US", options)
        } else {
            return new Date(key.date).toLocaleDateString("en-US", {
                weekday: 'short'
            })
        }
	}

	getTimeString(key) {
        if (key.dateTime) {
            return new Date(key.dateTime).toLocaleTimeString("en-US", {
                hour: 'numeric',
                minute: 'numeric'
            })
        } else {
            return new Date(key.date).toLocaleTimeString("en-US", {
                hour: 'numeric',
                minute: 'numeric'
            })
        }
	}
}