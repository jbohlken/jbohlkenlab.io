import '@babel/polyfill';

import 'jquery-pjax';
import 'owl.carousel';

import './default';
import './createslick';
import './createowl';