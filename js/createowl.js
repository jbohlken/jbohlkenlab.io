export default class CreateOwl {
	constructor() {
		this.$carousel = $('.owl-carousel');
		if (this.$carousel.length) {
			this.onLoad();
		}
	}

	onLoad() {
		this.$carousel.owlCarousel({
			items: 1,
			loop: true,
			margin: 10,
			nav: false,
			dots: false,
			autoplay: true
		});
	}
}
