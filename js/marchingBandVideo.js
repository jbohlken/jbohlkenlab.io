export default class MarchingBandVideo {
    constructor(ytPromise) {
        this.tag = document.createElement('script');
        this.$firstScriptTag = $('script').get(0);
        this.$youtubeLightbox = $('#youtube-lightbox');

        this.$youtubeLightbox.on('click', $.proxy(this.hideLightbox, this));

        ytPromise.then(this.createLightbox());
    }

    onLoad() {
        this.tag.src = 'https://www.youtube.com/iframe_api';
        this.$firstScriptTag.parentNode.insertBefore(this.tag, this.$firstScriptTag);
    }

    createYoutubePlayer(url) {
        this.player = new YT.Player('player-div', {
            videoId: url,
            playerVars: {
                autoplay: 1,
                modestbranding: 1,
                rel: 0,
                controls: 1
            },
            events: {
                onStateChange: $.proxy(this.onStateChange, this)
            }
        });
    }

    hideLightbox(event) {
        if (event.currentTarget !== $('.centered-child')) {
            const self = this;

            this.$youtubeLightbox.fadeOut(400, () => {
                self.$youtubeLightbox.find('iframe').remove();
                self.$youtubeLightbox.find('.video-wrapper').append('<div id="player-div"></div>');
            });
        }
    }

    onStateChange(event) {
        if (event.data === 0) {
            this.$youtubeLightbox.fadeOut(400);
            this.$youtubeLightbox.find('iframe').remove();
            this.$youtubeLightbox.find('.video-wrapper').append('<div id="player-div"></div>');
        }
    }

    createLightbox() {
        const self = this;

        $('body').on('click.YoutubeLightbox', 'a.lightbox', function(event) {
            event.preventDefault();

            const url = $(this).data('url');

            self.createYoutubePlayer(url);

            $('#youtube-lightbox').fadeIn(400);
        });
    }
}