export default class CreateSlick {
	constructor() {
		this.$slider = $('.slider').not('.slick-initialized');
		if (this.$slider.length) {
			$(document).ready(() => {
				$(window).on('resize', this.onLoad());
			});
			this.onLoad();
		}
	}

	onLoad() {
		this.$slider.slick({
			vertical: true,
			autoplay: true,
			autoplaySpeed: 3000,
			arrows: false,
			pauseOnHover: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			responsive: [
				{
					breakpoint: 576,
					settings: {
						vertical: false,
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
	}
}