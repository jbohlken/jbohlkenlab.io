var gulp = require('gulp');
var browserSyncBand = require('browser-sync').create();
var browserSyncMb = require('browser-sync').create();
var browserSyncHs = require('browser-sync').create();
var browserSyncMs = require('browser-sync').create();
var sass = require('gulp-ruby-sass');
var gutil = require('gulp-util');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var run = require('gulp-run');
var runSequence = require('run-sequence');
var sourcemaps = require('gulp-sourcemaps');
var webpack = require('webpack');
var webpackStream = require('webpack-stream');
var webpackConfig = require('./webpack.config.js');
var deployWebpackConfig = require('./deploy.webpack.config.js');
var del = require('del');
var notify = require('gulp-notify');
var { writeBoosterMinutesYaml } = require('./utilities/writeBoosterMinutesYaml');
var jsonToYaml = require('gulp-json-to-yaml');
var rename = require('gulp-rename');

var paths = require('./paths');

// User fs to write a .yml file dynamically to the booster minutes folder
gulp.task('build:boosterMinutes:band', function () {
	del('./band/_data/minutes.yml');

    writeBoosterMinutesYaml(
    	'./band/assets/documents/boosters/minutes',
		'minutes.json'
	);

    gulp.src('./minutes.json')
		.pipe(jsonToYaml())
		.pipe(rename('minutes.yml'))
		.pipe(gulp.dest('./band/_data/'))
		.on('error', gutil.log);

    return del('./minutes.json');
});

// Uses Sass compiler to process styles, add vendor prefixes, minify, and output to appropriate locations
gulp.task('deploy:styles:all', function() {
	return runSequence(['deploy:styles:band', 'deploy:styles:mb', 'deploy:styles:hs', 'deploy:styles:ms']);
});

gulp.task('build:styles:all', function() {
	return runSequence(['build:styles:band', 'build:styles:mb', 'build:styles:hs', 'build:styles:ms']);
});

gulp.task('build:styles:band', function() {
	return sass(paths.assetSassFiles + '/main.scss', {
		style: 'compressed',
		trace: true,
		sourcemap: true
	})
		.pipe(autoprefixer({browsers: ['last 2 versions', 'ie >= 10']}))
		.pipe(cleanCSS())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.bandDir))
		.pipe(gulp.dest(paths.bandSiteDir))
		.pipe(browserSyncBand.stream())
		.on('error', gutil.log);
});

gulp.task('build:styles:mb', function() {
	return sass(paths.assetSassFiles + '/main.scss', {
		style: 'compressed',
		trace: true,
		sourcemap: true
	})
		.pipe(autoprefixer({browsers: ['last 2 versions', 'ie >= 10']}))
		.pipe(cleanCSS())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.mbDir))
		.pipe(gulp.dest(paths.mbSiteDir))
		.pipe(browserSyncMb.stream())
		.on('error', gutil.log);
});

gulp.task('build:styles:hs', function() {
	return sass(paths.assetSassFiles + '/main.scss', {
		style: 'compressed',
		trace: true,
		sourcemap: true
	})
		.pipe(autoprefixer({browsers: ['last 2 versions', 'ie >= 10']}))
		.pipe(cleanCSS())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.hsDir))
		.pipe(gulp.dest(paths.hsSiteDir))
		.pipe(browserSyncHs.stream())
		.on('error', gutil.log);
});

gulp.task('build:styles:ms', function() {
	return sass(paths.assetSassFiles + '/main.scss', {
		style: 'compressed',
		trace: true,
		sourcemap: true
	})
		.pipe(autoprefixer({browsers: ['last 2 versions', 'ie >= 10']}))
		.pipe(cleanCSS())
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.msDir))
		.pipe(gulp.dest(paths.msSiteDir))
		.pipe(browserSyncMs.stream())
		.on('error', gutil.log);
});

gulp.task('deploy:styles:band', function() {
	return sass(paths.assetSassFiles + '/main.scss', {
		style: 'compressed',
		trace: true,
		sourcemap: false
	})
		.pipe(autoprefixer({browsers: ['last 2 versions', 'ie >= 10']}))
		.pipe(cleanCSS())
		.pipe(gulp.dest(paths.bandDir))
		.pipe(gulp.dest(paths.bandSiteDir))
		.pipe(browserSyncBand.stream())
		.on('error', gutil.log);
});

gulp.task('deploy:styles:mb', function() {
	return sass(paths.assetSassFiles + '/main.scss', {
		style: 'compressed',
		trace: true,
		sourcemap: false
	})
		.pipe(autoprefixer({browsers: ['last 2 versions', 'ie >= 10']}))
		.pipe(cleanCSS())
		.pipe(gulp.dest(paths.mbDir))
		.pipe(gulp.dest(paths.mbSiteDir))
		.pipe(browserSyncMb.stream())
		.on('error', gutil.log);
});

gulp.task('deploy:styles:hs', function() {
	return sass(paths.assetSassFiles + '/main.scss', {
		style: 'compressed',
		trace: true,
		sourcemap: false
	})
		.pipe(autoprefixer({browsers: ['last 2 versions', 'ie >= 10']}))
		.pipe(cleanCSS())
		.pipe(gulp.dest(paths.hsDir))
		.pipe(gulp.dest(paths.hsSiteDir))
		.pipe(browserSyncHs.stream())
		.on('error', gutil.log);
});

gulp.task('deploy:styles:ms', function() {
	return sass(paths.assetSassFiles + '/main.scss', {
		style: 'compressed',
		trace: true,
		sourcemap: false
	})
		.pipe(autoprefixer({browsers: ['last 2 versions', 'ie >= 10']}))
		.pipe(cleanCSS())
		.pipe(gulp.dest(paths.msDir))
		.pipe(gulp.dest(paths.msSiteDir))
		.pipe(browserSyncMs.stream())
		.on('error', gutil.log);
});

gulp.task('clean:styles:all', function() {
	return runSequence(['clean:styles:band', 'clean:styles:mb', 'clean:styles:hs', 'clean:styles:ms']);
});

gulp.task('clean:styles:band', function() {
	return del([
		paths.bandDir + 'main.css',
		paths.bandDir + 'main.css.map'
	]);
});

gulp.task('clean:styles:mb', function() {
	return del([
		paths.mbDir + 'main.css',
		paths.mbDir + 'main.css.map'
	]);
});

gulp.task('clean:styles:hs', function() {
	return del([
		paths.hsDir + 'main.css',
		paths.hsDir + 'main.css.map'
	]);
});

gulp.task('clean:styles:ms', function() {
	return del([
		paths.msDir + 'main.css',
		paths.msDir + 'main.css.map'
	]);
});

// Concats and uglifies JS files and outputs result to locations
gulp.task('deploy:scripts:all', function() {
	return gulp.src(paths.assetJsFiles + '/entry.js')
		.pipe(webpackStream(deployWebpackConfig), webpack)
		.pipe(gulp.dest(paths.bandDir))
		.pipe(gulp.dest(paths.bandSiteDir))
		.pipe(gulp.dest(paths.mbDir))
		.pipe(gulp.dest(paths.mbSiteDir))
	    .pipe(gulp.dest(paths.hsDir))
		.pipe(gulp.dest(paths.hsSiteDir))
	    .pipe(gulp.dest(paths.msDir))
		.pipe(gulp.dest(paths.msSiteDir))
		.on('error', gutil.log);
});

gulp.task('build:scripts:all', function() {
    return gulp.src(paths.assetJsFiles + '/entry.js')
		.pipe(webpackStream(webpackConfig), webpack)
        .pipe(gulp.dest(paths.bandDir))
		.pipe(gulp.dest(paths.bandSiteDir))
        .pipe(gulp.dest(paths.mbDir))
		.pipe(gulp.dest(paths.mbSiteDir))
	    .pipe(gulp.dest(paths.hsDir))
		.pipe(gulp.dest(paths.hsSiteDir))
	    .pipe(gulp.dest(paths.msDir))
		.pipe(gulp.dest(paths.msSiteDir))
        .on('error', gutil.log);
});

gulp.task('deploy:scripts:band', function() {
	return gulp.src(paths.assetJsFiles + '/entry.js')
		.pipe(webpackStream(deployWebpackConfig), webpack)
		.pipe(gulp.dest(paths.bandDir))
		.pipe(gulp.dest(paths.bandSiteDir))
		.on('error', gutil.log);
});

gulp.task('build:scripts:band', function() {
	return gulp.src(paths.assetJsFiles + '/entry.js')
		.pipe(webpackStream(webpackConfig), webpack)
		.pipe(gulp.dest(paths.bandDir))
		.pipe(gulp.dest(paths.bandSiteDir))
		.on('error', gutil.log);
});

gulp.task('build:scripts:mb', function() {
	return gulp.src(paths.assetJsFiles + '/entry.js')
		.pipe(webpackStream(webpackConfig), webpack)
		.pipe(gulp.dest(paths.mbDir))
		.pipe(gulp.dest(paths.mbSiteDir))
		.on('error', gutil.log);
});

gulp.task('build:scripts:hs', function() {
	return gulp.src(paths.assetJsFiles + '/entry.js')
		.pipe(webpackStream(webpackConfig), webpack)
		.pipe(gulp.dest(paths.hsDir))
		.pipe(gulp.dest(paths.hsSiteDir))
		.on('error', gutil.log);
});

gulp.task('build:scripts:ms', function() {
	return gulp.src(paths.assetJsFiles + '/entry.js')
		.pipe(webpackStream(webpackConfig), webpack)
		.pipe(gulp.dest(paths.msDir))
		.pipe(gulp.dest(paths.msSiteDir))
		.on('error', gutil.log);
});

gulp.task('clean:scripts:all', function() {
	return runSequence(['clean:scripts:band', 'clean:scripts:mb', 'clean:scripts:hs', 'clean:scripts:ms']);
});

gulp.task('clean:scripts:band', function() {
	return del([
		paths.bandDir + 'main.bundle.js',
		paths.bandDir + 'vendors.bundle.js',
		paths.bandDir + 'manifest.bundle.js',
		paths.bandDir + 'main.bundle.js.map',
		paths.bandDir + 'vendors.bundle.js.map',
		paths.bandDir + 'manifest.bundle.js.map',
		paths.bandSiteDir + 'main.bundle.js',
		paths.bandSiteDir + 'vendors.bundle.js',
		paths.bandSiteDir + 'manifest.bundle.js',
		paths.bandSiteDir + 'main.bundle.js.map',
		paths.bandSiteDir + 'vendors.bundle.js.map',
		paths.bandSiteDir + 'manifest.bundle.js.map'
	]);
});

gulp.task('clean:scripts:mb', function() {
	return del([
		paths.mbDir + 'main.bundle.js',
		paths.mbDir + 'vendors.bundle.js',
		paths.mbDir + 'manifest.bundle.js',
		paths.mbDir + 'main.bundle.js.map',
		paths.mbDir + 'vendors.bundle.js.map',
		paths.mbDir + 'manifest.bundle.js.map',
		paths.mbSiteDir + 'main.bundle.js',
		paths.mbSiteDir + 'vendors.bundle.js',
		paths.mbSiteDir + 'manifest.bundle.js',
		paths.mbSiteDir + 'main.bundle.js.map',
		paths.mbSiteDir + 'vendors.bundle.js.map',
		paths.mbSiteDir + 'manifest.bundle.js.map'
	]);
});

gulp.task('clean:scripts:hs', function() {
	return del([
		paths.hsDir + 'main.bundle.js',
		paths.hsDir + 'vendors.bundle.js',
		paths.hsDir + 'manifest.bundle.js',
		paths.hsDir + 'main.bundle.js.map',
		paths.hsDir + 'vendors.bundle.js.map',
		paths.hsDir + 'manifest.bundle.js.map',
		paths.hsSiteDir + 'main.bundle.js',
		paths.hsSiteDir + 'vendors.bundle.js',
		paths.hsSiteDir + 'manifest.bundle.js',
		paths.hsSiteDir + 'main.bundle.js.map',
		paths.hsSiteDir + 'vendors.bundle.js.map',
		paths.hsSiteDir + 'manifest.bundle.js.map'
	]);
});

gulp.task('clean:scripts:ms', function() {
	return del([
		paths.msDir + 'main.bundle.js',
		paths.msDir + 'vendors.bundle.js',
		paths.msDir + 'manifest.bundle.js',
		paths.msDir + 'main.bundle.js.map',
		paths.msDir + 'vendors.bundle.js.map',
		paths.msDir + 'manifest.bundle.js.map',
		paths.msSiteDir + 'main.bundle.js',
		paths.msSiteDir + 'vendors.bundle.js',
		paths.msSiteDir + 'manifest.bundle.js',
		paths.msSiteDir + 'main.bundle.js.map',
		paths.msSiteDir + 'vendors.bundle.js.map',
		paths.msSiteDir + 'manifest.bundle.js.map'
	]);
});

gulp.task('deploy:jekyll:all', function() {
	var bandShellCommand = 'bundle exec jekyll build --config band_config.yml,localhost_config.yml';
	var mbShellCommand = 'bundle exec jekyll build --config mb_config.yml,localhost_config.yml';
	var hsShellCommand = 'bundle exec jekyll build --config hs_config.yml,localhost_config.yml';
	var msShellCommand = 'bundle exec jekyll build --config ms_config.yml,localhost_config.yml';

	return gulp.src("")
		.pipe(run(bandShellCommand, {}))
		.pipe(run(mbShellCommand, {}))
	    .pipe(run(hsShellCommand, {}))
	    .pipe(run(msShellCommand, {}))
		.on('error', gutil.log);
});

gulp.task('deploy:jekyll:band', function() {
	var bandShellCommand = 'bundle exec jekyll build --config band_config.yml,localhost_config.yml';

	return gulp.src("")
		.pipe(run(bandShellCommand, {}))
		.on('error', gutil.log);
});

gulp.task('deploy:jekyll:mb', function() {
	var mbShellCommand = 'bundle exec jekyll build --config mb_config.yml,localhost_config.yml';

	return gulp.src("")
		.pipe(run(mbShellCommand, {}))
		.on('error', gutil.log);
});

gulp.task('deploy:jekyll:hs', function() {
	var hsShellCommand = 'bundle exec jekyll build --config hs_config.yml,localhost_config.yml';

	return gulp.src("")
		.pipe(run(hsShellCommand, {}))
		.on('error', gutil.log);
});

gulp.task('deploy:jekyll:ms', function() {
	var hsShellCommand = 'bundle exec jekyll build --config ms_config.yml,localhost_config.yml';

	return gulp.src("")
		.pipe(run(msShellCommand, {}))
		.on('error', gutil.log);
});

gulp.task('build:jekyll:all', function() {
	var bandShellCommand = 'bundle exec jekyll build --config band_config.yml,localhost_config.yml';
	var mbShellCommand = 'bundle exec jekyll build --config mb_config.yml,localhost_config.yml';
	var hsShellCommand = 'bundle exec jekyll build --config hs_config.yml,localhost_config.yml';
	var msShellCommand = 'bundle exec jekyll build --config ms_config.yml,localhost_config.yml';

	return gulp.src("")
	.pipe(run(bandShellCommand, {}))
	.pipe(run(mbShellCommand, {}))
	.pipe(run(hsShellCommand, {}))
	.pipe(run(msShellCommand, {}))
	.on('error', gutil.log);
});

gulp.task('build:jekyll:band', function() {
	var bandShellCommand = 'bundle exec jekyll build --config band_config.yml,localhost_config.yml';

	return gulp.src("")
		.pipe(run(bandShellCommand, {}))
		.on('error', gutil.log);
});

gulp.task('build:jekyll:mb', function() {
	var mbShellCommand = 'bundle exec jekyll build --config mb_config.yml,localhost_config.yml';

	return gulp.src("")
		.pipe(run(mbShellCommand, {}))
		.on('error', gutil.log);
});

gulp.task('build:jekyll:hs', function() {
	var hsShellCommand = 'bundle exec jekyll build --config hs_config.yml,localhost_config.yml';

	return gulp.src("")
		.pipe(run(hsShellCommand, {}))
		.on('error', gutil.log);
});

gulp.task('build:jekyll:ms', function() {
	var msShellCommand = 'bundle exec jekyll build --config ms_config.yml,localhost_config.yml';

	return gulp.src("")
		.pipe(run(msShellCommand, {}))
		.on('error', gutil.log);
});

gulp.task('clean:jekyll:all', function() {
	return del([paths.bandSiteDir, paths.mbSiteDir, , paths.hsSiteDir]);
});

gulp.task('clean:jekyll:band', function() {
	return del([paths.bandSiteDir]);
});

gulp.task('clean:jekyll:mb', function() {
	return del([paths.mbSiteDir]);
});

gulp.task('clean:jekyll:hs', function() {
	return del([paths.hsSiteDir]);
});

gulp.task('clean:jekyll:ms', function() {
	return del([paths.msSiteDir]);
});

gulp.task('clean:all', [
	'clean:jekyll:all',
	'clean:scripts:all',
	'clean:styles:all'
]);

gulp.task('clean:band', [
	'clean:jekyll:band',
	'clean:scripts:band',
	'clean:styles:band'
]);

gulp.task('clean:mb', [
	'clean:jekyll:mb',
	'clean:scripts:mb',
	'clean:styles:mb'
]);

gulp.task('clean:hs', [
	'clean:jekyll:hs',
	'clean:scripts:hs',
	'clean:styles:hs'
]);

gulp.task('clean:ms', [
	'clean:jekyll:ms',
	'clean:scripts:ms',
	'clean:styles:ms'
]);

gulp.task('build:all', function(cb) {
	runSequence(
		'clean:all',
		'build:boosterMinutes:band',
		['build:scripts:all', 'build:styles:all'],
		'build:jekyll:all',
		cb
	);
});

gulp.task('build:band', function(cb) {
	runSequence(
		'clean:band',
		'build:boosterMinutes:band',
		['build:scripts:band', 'build:styles:band'],
		'build:jekyll:band',
		cb
	);
});

gulp.task('build:mb', function(cb) {
	runSequence(
		'clean:mb',
		['build:scripts:mb', 'build:styles:mb'],
		'build:jekyll:mb',
		cb
	);
});

gulp.task('build:hs', function(cb) {
	runSequence(
		'clean:hs',
		['build:scripts:hs', 'build:styles:hs'],
		'build:jekyll:hs',
		cb
	);
});

gulp.task('build:ms', function(cb) {
	runSequence(
		'clean:ms',
		['build:scripts:ms', 'build:styles:ms'],
		'build:jekyll:ms',
		cb
	);
});

gulp.task('deploy:all', function(cb) {
	runSequence('clean:all', 'build:boosterMinutes:band', ['deploy:scripts:all', 'deploy:styles:all'], 'deploy:jekyll:all', cb);
});

gulp.task('deploy:band', function(cb) {
	runSequence('clean:band', 'build:boosterMinutes:band', ['deploy:scripts:band', 'deploy:styles:band'], 'deploy:jekyll:band', cb);
});

// ??? should there be the above for mb, hs and ms  ????

gulp.task('build:jekyll:watch:all', ['build:jekyll:all'], function(cb) {
	browserSyncBand.reload();
	browserSyncMb.reload();
	browserSyncHs.reload();
	browserSyncMs.reload();
	cb();
});

gulp.task('build:jekyll:watch:band', ['build:jekyll:band'], function(cb) {
	browserSyncBand.reload();
	cb();
});

gulp.task('build:jekyll:watch:mb', ['build:jekyll:mb'], function(cb) {
	browserSyncMb.reload();
	cb();
});

gulp.task('build:jekyll:watch:hs', ['build:jekyll:hs'], function(cb) {
	browserSyncHs.reload();
	cb();
});

gulp.task('build:jekyll:watch:hs', ['build:jekyll:ms'], function(cb) {
	browserSyncMs.reload();
	cb();
});

gulp.task('build:scripts:watch:all', ['build:scripts:all'], function(cb) {
	browserSyncBand.reload();
	browserSyncMb.reload();
	browserSyncHs.reload();
	browserSyncMs.reload();
	cb();
});

gulp.task('build:scripts:watch:band', ['build:scripts:band'], function(cb) {
	browserSyncBand.reload();
	cb();
});

gulp.task('build:scripts:watch:mb', ['build:scripts:mb'], function(cb) {
	browserSyncMb.reload();
	cb();
});

gulp.task('build:scripts:watch:hs', ['build:scripts:hs'], function(cb) {
	browserSyncHs.reload();
	cb();
});

gulp.task('build:scripts:watch:ms', ['build:scripts:ms'], function(cb) {
	browserSyncMs.reload();
	cb();
});

gulp.task('serve:all', ['build:all'], function() {
	browserSyncBand.init({
		port: 4000,
		ui: {
			port: 4001
		},
		server: {
			baseDir: paths.bandSiteDir
		},
		logFileChanges: true,
		ghostMode: false
	});

	browserSyncMb.init({
		port: 5000,
		ui: {
			port: 5001
		},
		server: {
			baseDir: paths.mbSiteDir
		},
		logFileChanges: true,
		ghostMode: false
	});
	
	browserSyncHs.init({
		port: 5500,
		ui: {
			port: 5501
		},
		server: {
			baseDir: paths.hsSiteDir
		},
		logFileChanges: true,
		ghostMode: false
	});
	
	browserSyncMs.init({
		port: 5800,
		ui: {
			port: 5801
		},
		server: {
			baseDir: paths.msSiteDir
		},
		logFileChanges: true,
		ghostMode: false
	});
	
	// Watch site changes
	gulp.watch(['band_config.yml', 'mb_config.yml', 'hs_config.yml', 'ms_config.yml', 'localhost_config.yml'], ['build:jekyll:watch:all']);

	// Watch additions to the booster minutes folder
	gulp.watch('band/assets/boosters/minutes/**/*.*', ['build:boosterMinutes:band']);
	
	// Watch asset .scss files and pipe to browserSync
	gulp.watch('css/**/*.scss', ['build:styles:all']);

    // Watch app .js files
    gulp.watch('js/**/*.js', ['build:scripts:watch:all']);
	
	// Watch Jekyll html files
	gulp.watch(
		[
			'band/**/*.html',
			'!band/_site/**/*.*',
			'marching/**/*.html',
			'!marching/_site/**/*.*',
			'highschool/**/*.html',
			'!highschool/_site/**/*.*',
			'middleschool/**/*.html',
			'!middleschool/_site/**/*.*'
		],
		[
			'build:jekyll:watch:all'
		]
	);
	
	// Watch Jekyll data files
	gulp.watch(
		[
			'band/_data/**.*+(yml|yaml|csv|json)',
			'marching/_data/**.*+(yml|yaml|csv|json)',
			'highschool/_data/**.*+(yml|yaml|csv|json)',
			'middleschool/_data/**.*+(yml|yaml|csv|json)'
		],
		[
			'build:jekyll:watch:all'
		]
	);
});

gulp.task('serve:band', ['build:band'], function() {
	browserSyncBand.init({
		port: 4000,
		ui: {
			port: 4001
		},
		server: {
			baseDir: paths.bandSiteDir
		},
		logFileChanges: true,
		ghostMode: false
	});

	// Watch site changes
	gulp.watch(['band_config.yml', 'localhost_config.yml'], ['build:jekyll:watch:band']);

	// Watch asset .scss files and pipe to browserSync
	gulp.watch('css/**/*.scss', ['build:styles:band']);

	// Watch app .js files
	gulp.watch('js/**/*.js', ['build:scripts:watch:band']);

	// Watch Jekyll html files
	gulp.watch(['band/**/*.html', '!band/_site/**/*.*'], ['build:jekyll:watch:band']);

	// Watch Jekyll data files
	gulp.watch(['band/_data/**.*+(yml|yaml|csv|json)'], ['build:jekyll:watch:band']);
});

gulp.task('serve:mb', ['build:mb'], function() {
	browserSyncMb.init({
		port: 5000,
		ui: {
			port: 5001
		},
		server: {
			baseDir: paths.mbSiteDir
		},
		logFileChanges: true,
		ghostMode: false
	});

	// Watch site changes
	gulp.watch(['mb_config.yml', 'localhost_config.yml'], ['build:jekyll:watch:mb']);

	// Watch asset .scss files and pipe to browserSync
	gulp.watch('css/**/*.scss', ['build:styles:mb']);

	// Watch app .js files
	gulp.watch('js/**/*.js', ['build:scripts:watch:mb']);

	// Watch Jekyll html files
	gulp.watch(['marching/**/*.html', '!marching/_site/**/*.*'], ['build:jekyll:watch:mb']);

	// Watch Jekyll data files
	gulp.watch(['marching/_data/**.*+(yml|yaml|csv|json)'], ['build:jekyll:watch:mb']);
});

gulp.task('serve:hs', ['build:hs'], function() {
	browserSyncHs.init({
		port: 5500,
		ui: {
			port: 5501
		},
		server: {
			baseDir: paths.hsSiteDir
		},
		logFileChanges: true,
		ghostMode: false
	});

	// Watch site changes
	gulp.watch(['hs_config.yml', 'localhost_config.yml'], ['build:jekyll:watch:hs']);

	// Watch asset .scss files and pipe to browserSync
	gulp.watch('css/**/*.scss', ['build:styles:hs']);

	// Watch app .js files
	gulp.watch('js/**/*.js', ['build:scripts:watch:hs']);

	// Watch Jekyll html files
	gulp.watch(['highschool/**/*.html', '!highschool/_site/**/*.*'], ['build:jekyll:watch:hs']);

	// Watch Jekyll data files
	gulp.watch(['highschool/_data/**.*+(yml|yaml|csv|json)'], ['build:jekyll:watch:hs']);
});

gulp.task('serve:ms', ['build:ms'], function() {
	browserSyncHs.init({
		port: 5800,
		ui: {
			port: 5801
		},
		server: {
			baseDir: paths.msSiteDir
		},
		logFileChanges: true,
		ghostMode: false
	});

	// Watch site changes
	gulp.watch(['ms_config.yml', 'localhost_config.yml'], ['build:jekyll:watch:ms']);

	// Watch asset .scss files and pipe to browserSync
	gulp.watch('css/**/*.scss', ['build:styles:ms']);

	// Watch app .js files
	gulp.watch('js/**/*.js', ['build:scripts:watch:ms']);

	// Watch Jekyll html files
	gulp.watch(['middleschool/**/*.html', '!middleschool/_site/**/*.*'], ['build:jekyll:watch:ms']);

	// Watch Jekyll data files
	gulp.watch(['middleschool/_data/**.*+(yml|yaml|csv|json)'], ['build:jekyll:watch:ms']);
});

gulp.task('update:yarn', function() {
	return gulp.src('')
		.pipe(run('yarn install'))
		.on('error', gutil.log);
});

gulp.task('update:bundle', function() {
	return gulp.src('')
		.pipe(run('bundle install'))
		.pipe(run('bundle update'))
		.pipe(notify({ message: 'Bundle Update Complete' }))
		.on('error', gutil.log);
});

gulp.task('update', ['update:yarn', 'update:bundle'], function(cb) {
	runSequence('build', cb);
});

gulp.task('default', ['serve:all']);