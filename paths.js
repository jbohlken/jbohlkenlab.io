var paths = {};

paths.bandDir = 'band/';
paths.mbDir = 'marching/';
paths.hsDir = 'highschool/';
paths.msDir = 'middleschool/';
paths.bandSiteDir = paths.bandDir + '_site/';
paths.mbSiteDir = paths.mbDir + '_site/';
paths.hsSiteDir = paths.hsDir + '_site/';
paths.msSiteDir = paths.msDir + '_site/';

// Folder naming conventions
paths.scriptFolderName = 'js';
paths.stylesFolderName = 'css';

// Asset file locations
paths.assetSassFiles = paths.stylesFolderName;
paths.assetJsFiles = paths.scriptFolderName;

// Glob patterns by file type
paths.sassPattern = '/**/*.scss';
paths.jsPattern = '/**/*.js';

// App file globs
paths.assetSassFilesGlob = paths.assetSassFiles + paths.sassPattern;
paths.assetJsFilesGlob = paths.assetJsFiles + paths.jsPattern;
paths.assetJsEntry = paths.assetJsFiles + '/main.js';

module.exports = paths;