module.exports = {
    mode: 'development',
    bail: true,
    target: 'web',
    entry: "./js/entry.js",
    output: {
        filename: '[name].bundle.js',
    },
    devtool: 'source-map',
    stats: {
        colors: true,
        reasons: true
    },
    module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
                loader: "babel-loader"
			}
		]
    },
    resolve: {
        extensions: ['.js', '.es6']
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        },
        runtimeChunk: {
            name: 'manifest'
        }
    }
};