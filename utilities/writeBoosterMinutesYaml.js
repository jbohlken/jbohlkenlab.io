const fs = require('fs');
const path = require('path');

const relationships = [
    {
        monthString: '01',
        month: 'January'
    },
    {
        monthString: '02',
        month: 'February'
    },
    {
        monthString: '03',
        month: 'March'
    },
    {
        monthString: '04',
        month: 'April'
    },
    {
        monthString: '05',
        month: 'May'
    },
    {
        monthString: '06',
        month: 'June'
    },
    {
        monthString: '07',
        month: 'July'
    },
    {
        monthString: '08',
        month: 'August'
    },
    {
        monthString: '09',
        month: 'September'
    },
    {
        monthString: '10',
        month: 'October'
    },
    {
        monthString: '11',
        month: 'November'
    },
    {
        monthString: '12',
        month: 'December'
    }
];

const writeBoosterMinutesYaml = function(filePath, fileName) {
    const fileNames = fs.readdirSync(filePath);

    let fileArray = [];

    for (const fileName of fileNames) {
        const ext = path.extname(fileName);

        if (ext !== '.pdf') {
            continue;
        }

        const year = fileName.substr(0, 4);
        const monthString = fileName.substr(5, 2);

        const relationship = relationships.find(relationship => relationship.monthString === monthString);

        const date = Date.parse(`${relationship.month} 1, ${year}`);

        const obj = {
            date: date,
            name: `${relationship.month} ${year}`,
            path: fileName
        };

        fileArray.push(obj);
    }

    fileArray.sort((a, b) => a.date < b.date ? 1 : -1);

    fileArray = fileArray.map(item => {
        return {
            name: item.name,
            path: item.path
        }
    });

    fs.writeFile(fileName, JSON.stringify(fileArray), err => {
        if (err) {
            console.error(err);
            return;
        }

        console.log(`${fileName} has been created`);
    })
};

module.exports = {
    writeBoosterMinutesYaml
};